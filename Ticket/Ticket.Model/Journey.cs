﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ticket.Model
{
    public class Journey
    {

        public DateTime Start { get; set; }
        public int StartCityId { get; set; }
        public int EndCityId { get; set; }
    }
}
