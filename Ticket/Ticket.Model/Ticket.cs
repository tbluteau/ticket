﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ticket.Model
{
    public class Ticket
    {
        public int Id { get; set; }
        public int JourneyId { get; set; }
        public string PassengerName{ get; set; }

    }
}
