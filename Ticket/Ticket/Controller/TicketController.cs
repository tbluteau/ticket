﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ticket.DAL;

namespace Ticket.Controllers
{
    public class CityController : Controller
    {
        TicketController _ctrl;
        public CityController()
        {
            _ctrl = new TicketController(new TicketDALFile());
        }
        
        public string Index()
        {
            return "hello world";
        }

        public string[] Cities()
        {
           return _ctrl.GetAllCities().Select(c => c.Name).ToArray();
        }

        public string[] Search(string search)
        {
            return _ctrl.SearchCities(search).Select(c => c.Name).ToArray();
        }
    }
}
