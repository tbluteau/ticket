﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using Ticket.IDAL;
using Ticket.Model;

namespace Ticket.DAL
{
    public class TicketDALFile : TicketIDAL
    {
        private ICollection<Journey> _journeys = new List<Journey>();

        private ICollection<City> _cities = new List<City>();

        private ICollection<Ticket.Model.Ticket> _tickets = new List<Ticket.Model.Ticket>();

        public TicketDALFile()
        {
            string filename = @"./DAL/db.json";

            
            // read JSON directly from a file
            using (StreamReader file = File.OpenText(filename))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                
                JObject o = (JObject)JToken.ReadFrom(reader);
                JArray arrayJourneys = (JArray)o["journeys"];
                _journeys  = arrayJourneys.ToObject<IList<Journey>>();
                JArray arrayCities = (JArray)o["cities"];
                _cities = arrayCities.ToObject<IList<City>>();
                JArray arrayTicket = (JArray)o["tickets"];
                _tickets  =arrayTicket.ToObject<IList<Ticket.Model.Ticket>>();
            }

        }
        public ICollection<Journey> Journeys => _journeys;

        public ICollection<City> Cities => _cities;

        public ICollection<Model.Ticket> Tickets => _tickets;
    }
}
