﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ticket.IDAL;
using Ticket.Model;

namespace Ticket.Controllers
{
    public class TicketController
    {
        private TicketIDAL _dal;
        public TicketController(TicketIDAL dal)
        {
            _dal = dal; 
        }

        public ICollection<City> GetAllCities()
        {
            return _dal.Cities;
        }

        public ICollection<City> SearchCities(string search)
        {
            return _dal.Cities.Where(v => v.Name.Contains(search)).ToList();
        }
    }
}
