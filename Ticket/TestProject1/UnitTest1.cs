

using Ticket.Controllers;
using Ticket.IDAL;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        TicketIDAL _mockedDAL; 

        public UnitTest1()
        {
            _mockedDAL = new TicketDALMocked();
        }

        [TestMethod]
        public void TestGetAllCities()
        {
            var controller = new TicketController(_mockedDAL);
            Assert.AreEqual(3, controller.GetAllCities().Count);
        }

        [TestMethod]
        public void TestSearchCities()
        {
            var controller = new TicketController(_mockedDAL);
            Assert.AreEqual(2, controller.SearchCities("ille").Count);
        }

    }
}