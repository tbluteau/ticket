﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ticket.IDAL;
using Ticket.Model;

namespace TestProject1
{
    internal class TicketDALMocked : TicketIDAL
    {
        private ICollection<Journey> _journeys = new List<Journey>();

        private ICollection<City> _cities = new List<City>();

        private ICollection<Ticket.Model.Ticket> _tickets = new List<Ticket.Model.Ticket>();

        public TicketDALMocked()
        {
            _cities.Add(new City() { Id = 1, Name = "Paris" });
            _cities.Add(new City() { Id = 2, Name = "Lille" });
            _cities.Add(new City() { Id = 3, Name = "Marseille" });
            var offsetToTuesday = DayOfWeek.Tuesday - DateTime.Now.DayOfWeek;
            offsetToTuesday += (offsetToTuesday <= 0 ) ? 7 : 0;
            var nextTuesday = DateTime.Now.Date.AddDays(offsetToTuesday).AddHours(10).AddMinutes(30);
            _journeys.Add(new Journey() { Start = nextTuesday, StartCityId = _cities.ElementAt(0).Id, EndCityId = _cities.ElementAt(1).Id });     
        }
        

        public ICollection<Journey> Journeys { get { return _journeys; } }
        public ICollection<City> Cities { get { return _cities; }  }
        public ICollection<Ticket.Model.Ticket> Tickets { get { return _tickets; }  }
        
    }
}
