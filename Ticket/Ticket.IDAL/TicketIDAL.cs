﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ticket.Model;

namespace Ticket.IDAL
{
    public interface TicketIDAL
    {
        ICollection<Journey> Journeys { get; }
        ICollection<City> Cities { get; }

        ICollection<Ticket.Model.Ticket> Tickets { get; }
    }
}
